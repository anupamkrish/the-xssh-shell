#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFLEN 128
#define INSNUM 8
/*internal instructions*/
char *instr[INSNUM] = {"echo", "set", "export", "unexport", "echo", "exit", "wait", "help", "hi"};
/*predefined variables*/
/*varvalue[0] stores the rootpid of xssh*/
/*varvalue[3] stores the childpid of the last process that was executed by xssh in the background*/
int varmax = 3;
char varname[BUFLEN][BUFLEN] = {"$\0", "?\0", "!\0", '\0'};
char varvalue[BUFLEN][BUFLEN] = {'\0', '\0', '\0'};
/*remember pid*/
int childnum = 0;
pid_t childpid = 0;
pid_t rootpid = 0;
/*current dir*/
char rootdir[BUFLEN] = "\0";

/*functions for parsing the commands*/
int deinstr(char buffer[BUFLEN]);
void substitute(char *buffer);

/*functions supported*/
int xsshexit(char buffer[BUFLEN]);
void echo(char buffer[BUFLEN]);
void help(char buffer[BUFLEN]);
int program(char buffer[BUFLEN]);
void catchctrlc();
void ctrlsig(int sig);
void waitchild(char buffer[BUFLEN]);
void set(char buffer[BUFLEN]);
void export(char buffer[BUFLEN]);
void unexport(char buffer[BUFLEN]);


// int pipeprog(char buffer[BUFLEN]);


/*main function*/
int main()
{
	/*set the variable $$*/
	rootpid = getpid();
	childpid = rootpid;
	sprintf(varvalue[0], "%d\0", rootpid);
	/*capture the ctrl+C*/
	catchctrlc();
	/*run the xssh, read the input instruction*/
	int xsshprint = 0;
	if (isatty(fileno(stdin)))
		xsshprint = 1;
	if (xsshprint){
		printf("__  __  \n");
		printf("\\ \\/ /  __   __   _   _\n");
		printf(" \\  /  / _/ / _/ | |_| |\n");
		printf(" /  \\  \\_ \\ \\_ \\ |  _  |\n");
		printf("/_/\\_\\ /__/ /__/ |_| |_|\n");
		printf("Welcome to XSSH Shell v1.0.\n");
		printf("Type 'help' to see supported commands.\n\n");
		printf("xssh>> ");
	}
	char buffer[BUFLEN];
	while (fgets(buffer, BUFLEN, stdin) > 0)
	{
		/*substitute the variables*/
		substitute(buffer);
		/*delete the comment*/
		char *p = strchr(buffer, '#');
		if (p != NULL)
		{
			*p = '\n';
			*(p + 1) = '\0';
		}
		/*decode the instructions*/
		int ins = deinstr(buffer);
		/*run according to the decoding*/
		if (ins == 1)
			echo(buffer);
		else if (ins == 2)
			set(buffer);
		else if (ins == 3)
			export(buffer);
		else if (ins == 4)
			unexport(buffer);
		else if (ins == 5)
			echo(buffer); //Not used for now
		else if (ins == 6)
			xsshexit(buffer);
		else if (ins == 7)
			waitchild(buffer);
		else if (ins == 8)
			help(buffer);
		else if (ins == 9)
			continue;
		else
		{
			char *ptr = strchr(buffer, '|');
			if (ptr != NULL)
			{
				int err = pipeprog(buffer);
				if (err != 0)
					break;
			}
			else
			{
				int err = program(buffer);
				if (err != 0)
					break;
			}
		}
		if (xsshprint)
			printf("xssh>> ");
		memset(buffer, 0, BUFLEN);
	}
	return -1;
}

/*exit I*/
int xsshexit(char buffer[BUFLEN])
{
	//exits with a return value I that is stored in buffer
	//trick is to find the start of the string of return value I
	int x;
	x = strlen(buffer);

	if (x > 5)
	{
		if (buffer[x - 2] != ' ')
		{
			printf("%s", &buffer[5]);
			exit(0);
		}
		else
		{
			printf("-1\n");
			exit(1);
		}
	}
	else if (x == 5)
	{
		printf("0\n");
		exit(0);
	}
}

/*echo W*/
void echo(char buffer[BUFLEN])
{
	//print the string after "echo " in buffer
	//trick is to find the start of this string
	printf("%s", &buffer[5]); // any string followed by "echo " starts at the 5th position
}

/*help*/
void help(char buffer[BUFLEN])
{
	printf("\n__  __  \n");
	printf("\\ \\/ /  __   __   _   _\n");
	printf(" \\  /  / _/ / _/ | |_| |\n");
	printf(" /  \\  \\_ \\ \\_ \\ |  _  |\n");
	printf("/_/\\_\\ /__/ /__/ |_| |_|\n");
	printf("Welcome to XSSH Shell v1.0.\n\n");
	//list of commands that your shell supports
	printf("These shell commands are defined internally. Type 'help' to see this list.\n\n exit [I]\n echo [W]\n help\n export [W]\n unexport [W]\n set [W1] [W2]\n wait [P]\n sleep\n CTRL-C handler\n\n");
	printf("1. Supports UNIX-style pipe C1 | C2 | C3\n\tUsage:pwd | ls | head -1\n2. Supports 'stdin/stdout' redirection\n\tManual:'C < F1 > F2', where it will only have either '<' or '>', but if it has both then the order will be '<' before '>'.\n\tAlso, the file should already exist in order to open it. If the file does not exist, it will print an error message.\n\tUsage:'vi log1.txt; vi log2.txt; cat < log1.txt > log2.txt'\n");
}

/*export variable --- set the variable name in the varname list*/
void export(char buffer[BUFLEN])
{
	int i, j;
	//flag == 1, if variable name exists in the varname list
	int flag = 0;
	//parse and store the variable name in buffer[]
	char str[BUFLEN];
	int start = 7;
	while (buffer[start] == ' ')
		start++;
	for (i = start; (i < strlen(buffer)) && (buffer[i] != '#') && (buffer[i] != ' ') && (buffer[i] != '\n'); i++)
	{
		str[i - start] = buffer[i];
	}
	str[i - start] = '\0';
	//trick is to try to print "str" and "varname[j]" to see what's stored there
	for (j = 0; j < varmax; j++)
	{
		//if the variable name (in "str") exists in the
		//varname list (in "varname[j]"), set the flag to be 1
		//using strcmp()
		if (strcmp(varname[j], str) == 0)
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0) //variable name does not exist in the varname list
	{
		//copy the variable name to "varname[varmax]" using strcpy()
		strcpy(varname[varmax], str);
		//set the corresponding value in "varvalue[varmax]" to empty string '\0'
		strcpy(varvalue[varmax], "\0");
		//update the 'varmax' (by +1)
		varmax += 1;
		//print "-xssh: Export variable str.", where str is newly exported variable name
		printf("-xssh: Export variable %s\n", str);

		for (int x = 0; x < varmax; x++)
		{
			printf("%s\n", varname[x]);
		}
	}
	else //variable name already exists in the varname list
	{
		//print "-xssh: Existing variable str is value.", where str is newly exported variable name and value is its corresponding value (stored in varvalue list)
		printf("-xssh: Existing variable %s is %d.\n", str, varvalue[j]);
	}
}

/*unexport the variable --- remove the variable name in the varname list*/
void unexport(char buffer[BUFLEN])
{
	int i, j;
	//flag == 1, if variable name exists in the varname list
	int flag = 0;
	//parse and store the variable name in buffer[]
	char str[BUFLEN];
	int start = 9;
	while (buffer[start] == ' ')
		start++;
	for (i = start; (i < strlen(buffer)) && (buffer[i] != '#') && (buffer[i] != ' ') && (buffer[i] != '\n'); i++)
	{
		str[i - start] = buffer[i];
	}
	str[i - start] = '\0';
	for (j = 0; j < varmax; j++)
	{
		//if the variable name (in "str") exists in the
		//varname list (in "varname[j]"), set the flag to 1
		//using strcmp() --- same with export()
		if (strcmp(varname[j], str) == 0)
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0) //variable name does not exist in the varname list
	{
		//print "-xssh: Variable str does not exist.",
		//where str is the variable name to be unexported
		printf("-xssh: Variable %s does not exist.\n", str);
	}
	else //variable name already exists in the varname list
	{
		//clear the found variable by setting its
		//"varname" and "varvalue" both to '\0'
		for (int x = 0; x < varmax; x++)
		{ // find the variable by looping through varnames and nullify its value.
			if (strcmp(varname[x], str) == 0)
			{							  // matching the entered variable to existing variables.
				strcpy(varname[x], "\0"); // nullify its value.
				break;
			}
			// printf("%s\n",varname[x]);  // uncomment to print
		}
		varvalue[j][0] = '\0'; // nullify corresponding varvalue.
		//print "-xssh: Variable str is unexported.",
		printf("-xssh: Variable %s is unexported.\n", str);
		//where str is the variable name to be unexported
	}
}

/*set the variable --- set the variable value for the given variable name*/
void set(char buffer[BUFLEN])
{
	int i, j;
	//flag == 1, if variable name exists in the varname list
	int flag = 0;
	//parse and store the variable name in buffer[]
	char str[BUFLEN];
	int start = 4;
	while (buffer[start] == ' ')
		start++;
	for (i = start; (i < strlen(buffer)) && (buffer[i] != ' ') && (buffer[i] != '#'); i++)
	{
		str[i - start] = buffer[i];
	}
	str[i - start] = '\0';
	while (buffer[i] == ' ')
		i++;
	if (buffer[i] == '\n')
	{
		printf("No value to set!\n");
		return;
	}
	for (j = 0; j < varmax; j++)
	{
		//if the variable name (in "str") exists in the
		//varname list (in "varname[j]"), set the flag to 1
		//using strcmp() --- same with export()
		if (strcmp(varname[j], str) == 0)
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		//print "-xssh: Variable str does not exist.",
		printf("-xssh: Variable str does not exist.");
		//where str is the variable name to be unexported
	}
	else
	{
		//trick is to to print "buffer[i]" to see what's stored there
		// printf("%s\n", buffer + i);

		//may need to add '\0' by the end of a string
		//set the corresponding varvalue to be value (in buffer[i]) using strcpy()
		strcpy(varvalue[j], buffer + i);
		varvalue[j][strlen(buffer + i)] = '\0';

		//print "-xssh: Set existing variable str to value.", where str is newly
		// exported variable name and value is its corresponding value (stored in varvalue list)
		printf("-xssh: Set existing variable %s to %s.\n", str, varvalue[j]);
	}
}

/*catch the ctrl+C*/
void catchctrlc()
{
	//catch the ctrl+C signal
	// (!(signal(SIGINT, ctrlsig) == SIG_ERR)) ?: exit(0);
	if (signal(SIGINT, ctrlsig) == SIG_ERR){
		exit(0);
	}
}

/*ctrl+C handler*/
void ctrlsig(int sig)
{
	if (childpid != rootpid)
	{
		int is_killed = kill(childpid, SIGKILL);
		if (is_killed == 0){
			printf("Exit pid %d\n", childpid);
			childpid = rootpid; // kill process
		} else {
			printf("Error: Unable to kill process, exiting shell.\n");
			exit(1); // can't kill, so exits shell.
		}
		// (is_killed == 0) ? (printf("Exit pid %d\n", childpid),
		// 					childpid = rootpid)													 // kill process
		// 				 : (printf("Error: Unable to kill process, exiting shell.\n"), exit(1)); //can't kill, so exits shell.
	}
	fflush(stdout);
	//first check if the foreground process (pid stored in childpid) is xssh itself (pid stored in rootpid)
	//if it is not xssh itself, then print "-xssh: Exit pid childpid", where childpid is the pid of the current process
}

/*wait instruction*/
void waitchild(char buffer[BUFLEN])
{
	int i;
	int start = 5;

	/*store the childpid in pid*/
	char number[BUFLEN] = {'\0'};
	while (buffer[start] == ' ')
		start++;
	for (i = start; (i < strlen(buffer)) && (buffer[i] != '\n') && (buffer[i] != '#'); i++)
	{
		number[i - start] = buffer[i];
	}
	number[i - start] = '\0';
	char *endptr;
	int pid = strtol(number, &endptr, 10);

	int status;
	pid_t wait_status = waitpid(pid, &status, WNOHANG);
	/*simple check to see if the input is valid or not*/
	if ((*number != '\0') && (*endptr == '\0'))
	{
		//if pid is not -1, try to wait the background process pid
		if (!status)
		{
			if (pid == -1)
			{
				//trick: remember to set the childnum correctly after waiting!
				childnum--;
				printf("-xssh: wait %d background processes\n", childnum);
			}
			else if (wait_status == -1)
			{
				printf("-xsshError: Unable to wait process %d\n", pid);
				return;
			}
			else
			{
				printf("-xssh: Have finished waiting process %d\n", pid);
			}
		}
		else
		{
			printf("-xssh: Unsuccessfully wait the background process %d\n", pid);
		}
	}
	else
		printf("-xssh: wait: Invalid pid\n");
}

//fn for parsing pipe
char **parsePipe(char buffer[BUFLEN])
{
	int i = 0;
	//no of arguments
	int n = 2;

	while (buffer[i])
	{
		(!(buffer[i] == '|')) ?: n++;
		i++;
	}

	int params = 0;
	char **args = (char **)malloc(sizeof(char *) * n + 1);
	char *catch = strtok(buffer, "|");

	while (catch != NULL)
	{
		args[params] = (char *)malloc(sizeof(char) * strlen(catch));
		strcpy(args[params], catch);
		catch = strtok(NULL, "|");
		params++;
	}

	args[n] = (char *)0;
	return args;
}

//fn for parsing space
char **str_split(char buffer[BUFLEN])
{
	int i = 0;
	//no of arguments
	int n = 1;
	while (buffer[i])
	{
		(!(buffer[i] == ' ')) ?: n++;
		i++;
	}

	char *catch = strtok(buffer, " ");
	char **args = (char **)malloc(sizeof(char *) * n + 1);
	int params = 0;

	while (catch != NULL)
	{
		args[params] = (char *)malloc(sizeof(char) * strlen(catch));
		strcpy(args[params], catch);
		catch = strtok(NULL, " ");
		params++;
	}
	args[n] = (char *)0;
	return args;
}

/*execute the external command*/
int program(char buffer[BUFLEN])
{
	/*if backflag == 0, xssh need to wait for the external command to complete*/
	/*if backflag == 1, xssh need to execute the external command in the background*/
	int backflag = 0;
	char *ptr = strchr(buffer, '&');
	if (ptr != NULL)
	{
		backflag = 1;
		*(ptr) = ' ';
	}

	pid_t pid;
	//creates a new process for executing the external command
	pid = fork();
	//remembers to check if the process creation is successful or not. if not, print error message and return -2, see codes below;
	if (pid < 0)
	{
		printf("Could not create child process");
		return -2;
	}
	//execute the external command in the newly created process, using execvp()
	//the external command is stored in buffer, but before execute it you may need to do some basic validation check or minor changes, depending on how you execute
	//remember to check if the external command is executed successfully; if not, prints error message
	//after executing the extenal command using execvp(), you need to return -1;
	/*for stdin/stdout redirection, find code below*/

	int right = 0;
	int left = 0;

	int o = 0;
	while (buffer[o])
	{
		if (buffer[o] == '<')
			left = o;
		if (buffer[o] == '>')
			right = o;
		o++;
	}

	if (pid == 0)
	{
		char *start = buffer;
		while (*start == ' ')
			start++;

		//removing new line
		start[strlen(start) - 1] = '\0';

		char **command = str_split(start);
		int q = 0;

		int redirect()
		{
			while (command[q])
			{

				(strcmp(command[q], "<") == 0) ? (
													 close(0),
													 open(command[q + 1], O_RDONLY),
													 strcpy(command[q], " "),
													 strcpy(command[q + 1], " "))
											   : (
													 (!(strcmp(command[q], ">") == 0)) ?: (close(1), open(command[q + 1], O_WRONLY | O_TRUNC), strcpy(command[q], " "), strcpy(command[q + 1], " ")));
				q++;
			}
		}

		(!(left || right)) ?: redirect();

		//Programs give error if space is an argument
		int x = 0;
		while (x < q)
		{
			(!(strcmp(command[x], " ") == 0)) ?: (command[x] = (char *)0);
			x++;
		}
		execvp(command[0], command);
	}

	//in the xssh process, remember to act differently, based on whether backflag is 0 or 1
	// childnum++;
	// childpid = pid;
	// childnum--; //temporary: this may or may not be needed
	if (backflag == 1)
	{
		printf("%d\n", pid);
		sprintf(varvalue[3], "%d\0", pid);
		childnum += 1;
	}
	else if (backflag == 0)
	{
		int status;
		wait(&status);
	}
	sprintf(varvalue[2], "%d\0", pid);
	return 0;
}

/*execute the pipe programs*/
int pipeprog(char buffer[BUFLEN])
{

	int n = 0;
	int x = 0;

	while (buffer[x])
	{
		if (buffer[x] == '|')
			n++;
		if (buffer[x] == '\n')
			buffer[x] = ' ';

		x++;
	}
	if (n == 0)
		return 0;

	char **programs = parsePipe(buffer);
	x = 0;
	char **prog[n + 2];
	while (programs[x])
	{
		prog[x] = str_split(programs[x]);
		x++;
	}
	prog[n + 1] = NULL;

	x = 0;
	int in = 0;
	int pid;

	int p1[n][2];
	int wait;

	while (x < n)
	{
		pipe(p1[x]);
		pid = fork();
		if (pid == 0)
		{
			dup2(p1[x][1], 1);
			close(p1[x][0]);
			dup2(in, 0);
			execvp(prog[x][0], prog[x]);
		}
		else
		{
			in = p1[x][0];
			waitpid(pid, &wait, WNOHANG);
		}
		x++;
	}

	pid = fork();
	if (pid == 0)
	{
		dup2(in, 0);
		execvp(prog[n][0], prog[n]);
		close(p1[n - 1][0]);
	}
	waitpid(pid, &wait, WNOHANG);

	return 0;
}

/*substitute the variable with its value*/
void substitute(char *buffer)
{
	char newbuf[BUFLEN] = {'\0'};
	int i;
	int pos = 0;
	for (i = 0; i < strlen(buffer); i++)
	{
		if (buffer[i] == '#')
		{
			newbuf[pos] = '\n';
			pos++;
			break;
		}
		else if (buffer[i] == '$')
		{
			if ((buffer[i + 1] != '#') && (buffer[i + 1] != ' ') && (buffer[i + 1] != '\n'))
			{
				i++;
				int count = 0;
				char tmp[BUFLEN];
				for (; (buffer[i] != '#') && (buffer[i] != '\n') && (buffer[i] != ' '); i++)
				{
					tmp[count] = buffer[i];
					count++;
				}
				tmp[count] = '\0';
				int flag = 0;
				int j;
				for (j = 0; j < varmax; j++)
				{
					if (strcmp(tmp, varname[j]) == 0)
					{
						flag = 1;
						break;
					}
				}
				if (flag == 0)
				{
					printf("-xssh: Does not exist variable $%s.\n", tmp);
				}
				else
				{
					strcat(&newbuf[pos], varvalue[j]);
					pos = strlen(newbuf);
				}
				i--;
			}
			else
			{
				newbuf[pos] = buffer[i];
				pos++;
			}
		}
		else
		{
			newbuf[pos] = buffer[i];
			pos++;
		}
	}
	if (newbuf[pos - 1] != '\n')
	{
		newbuf[pos] = '\n';
		pos++;
	}
	newbuf[pos] = '\0';
	strcpy(buffer, newbuf);
	//printf("Decode: %s", buffer);
}

/*decode the instruction*/
int deinstr(char buffer[BUFLEN])
{
	int i;
	int flag = 0;
	for (i = 0; i < INSNUM; i++)
	{
		flag = 0;
		int j;
		int stdlen = strlen(instr[i]);
		int len = strlen(buffer);
		int count = 0;
		j = 0;
		while (buffer[count] == ' ')
			count++;
		if ((buffer[count] == '\n') || (buffer[count] == '#'))
		{
			flag = 0;
			i = INSNUM;
			break;
		}
		for (j = count; (j < len) && (j - count < stdlen); j++)
		{
			if (instr[i][j] != buffer[j])
			{
				flag = 1;
				break;
			}
		}
		if ((flag == 0) && (j == stdlen) && (j <= len) && (buffer[j] == ' '))
		{
			break;
		}
		else if ((flag == 0) && (j == stdlen) && (j <= len) && (i == 5))
		{
			break;
		}
		else if ((flag == 0) && (j == stdlen) && (j <= len) && (i == 7))
		{
			break;
		}
		else
		{
			flag = 1;
		}
	}
	if (flag == 1)
	{
		i = 0;
	}
	else
	{
		i++;
	}
	return i;
}
