```  
__  __  
\ \/ /  __   __   _   _
 \  /  / _/ / _/ | |_| |
 /  \  \_ \ \_ \ |  _  |
/_/\_\ /__/ /__/ |_| |_|
```

# XSSH Shell
It can be argued that the main purpose of an OS is to simplify the execution of computer programs. A central part of this is the program that allows a user to execute other programs: on Unix-based Operating Systems this is the terminal, or, shell. The shell is not part of the operating system kernel, but it is a natural extension of it and in many ways represents the operating system itself! We must never take programs like *bash* or *csh* for granted. For this purpose, I built a custom rendition of shell from scratch. System calls, which is an important abstraction in any OS, is heavily implemented in this program. The shell is written completely in C.

Trivia: The *XSSH* name is inspired from my favourite [xfce interface](https://www.xfce.org/).

## What's in it?

The following shell commands are defined internally.
```
exit [I]
echo [W]
export [W]
unexport [W]
set [W1] [W2]
wait [P]
sleep
CTRL-C handler
help
```

To display full list of supported commands and get help, type
```
help
```

## What's new

Support for UNIX-style pipe *cmd1 | cmd2 | cmd3* originally written by Douglas Mcllroy.

**Usage**
```
pwd | ls | head -1
```
Support for 'stdin/stdout' redirection.

**Manual:**
C < F1 > F2', where it will only have either '<' or '>', but if it has both then '<' supersedes '>'

**Usage**
```
vi log1.txt; vi log2.txt; cat < log1.txt > log2.txt
```

Note that the file should already exist in order to open it. If the file does not exist, it will print an error message.

## Getting Started

These instructions will get you a copy of the compiler up and running on your terminal for development and testing purposes.

### Commands

The Makefile helps compile code. To compile, type:

```
make
```
To clean up the executable files, type:

```
make clean
```
To execute the xssh shell after make, type:
```
./xssh
```
To visit manual and see supported commands, type:
```
help
```
